# Flutter

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/AAkumaa/flutter.git
git branch -M main
git push -uf origin main
```

---

## Name

Akuma Project-.

## Description

This project is for simple coding purposes only. An attempt to get into Flutter development based on the Bloc structure.
My main goal is to write code that is StatefulWidget and turn it into a StatelessWidget. A challenge that could appeal to everyone.

## Usage

Please use it only for learning. The code is for us to create an amazing app that is very simple but also complicated at the same time. 
You can find each SatefulWidget implementation in the main file. I have commented out each main so you can see what the original was.

## Support

If you have any questions, please contect me at pestszil1997@gmail.com

## Roadmap

I will gradually add several projects to this project and create a small wallet for all those who wonder what it is like to first create an app with StatefulWidgets and then convert it to a StatelessWidget with Bloc functions. As the name suggests, this project will have 30 small projects over the development period.

## Authors and acknowledgment

Szilveszter Marcell Pesti

## License

Use to learn.

## Project status

Open
