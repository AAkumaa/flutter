import 'package:flutter/material.dart';

class ArrowShapeRight extends ShapeBorder {
  @override
  EdgeInsetsGeometry get dimensions => EdgeInsetsGeometry.infinity;

  @override
  Path getInnerPath(Rect rect, {TextDirection? textDirection}) {
    return Path();
  }

  @override
  Path getOuterPath(Rect rect, {TextDirection? textDirection}) {
    return Path()
      ..moveTo(rect.right, rect.top + rect.height / 2)
      ..lineTo(rect.left, rect.top)
      ..lineTo(rect.left, rect.bottom)
      ..close();
  }

  @override
  void paint(Canvas canvas, Rect rect, {TextDirection? textDirection}) {}

  @override
  ShapeBorder scale(double t) {
    return this;
  }
}
