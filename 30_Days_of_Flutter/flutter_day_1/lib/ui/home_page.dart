import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_day_1/bloc/bloc/function_bloc.dart';
import 'package:flutter_day_1/ui/arrow_shape_left.dart';
import 'package:flutter_day_1/ui/arrow_shape_right.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => FunctionBloc(),
      child: Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text('What do you want to eat?'),
              BlocBuilder(
                bloc: BlocProvider.of<FunctionBloc>(context),
                builder: (context, state) {
                  if (state is FunctionNextState ||
                      state is FunctionPrevState) {
                    state as FunctionState;
                    return Text(
                        'Current restaurant: ${state.restaurants[state.currentIndex]}');
                  } else {
                    return Container();
                  }
                },
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  FloatingActionButton(
                      shape: ArrowShapeLeft(),
                      mini: true,
                      onPressed: () {
                        BlocProvider.of<FunctionBloc>(context)
                            .add(FunctionPrevEvent());
                      }),
                  FloatingActionButton(
                    mini: true,
                    shape: ArrowShapeRight(),
                    onPressed: () {
                      BlocProvider.of<FunctionBloc>(context)
                          .add(FunctionNextEvent());
                    },
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
