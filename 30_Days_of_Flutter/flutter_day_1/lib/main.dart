import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_day_1/bloc/bloc/function_bloc.dart';
import 'package:flutter_day_1/ui/home_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => FunctionBloc(),
      child: const MaterialApp(
        home: HomePage(),
      ),
    );
  }
}

///Example code for StatefulWidget

/*class _MyAppState extends State<MyApp> {
  List<String> restaurants = [
    'McDonad\'s',
    'FooRocket',
    'KFC',
  ];

//Fehler behoben und dokumentiert
  int? currentIndex;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text('Waht do you want to eat?'),
            //Fehler behoben und dokumentiert
            if (currentIndex != null)
              Text(
                //Problemstelle
                restaurants[currentIndex.hashCode],
                style: const TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                ),
                textAlign: TextAlign.center,
              ),
            const Padding(padding: EdgeInsets.only()),
            FloatingActionButton(
              hoverElevation: 3.4,
              child: const Icon(
                Icons.navigate_next,
              ),
              onPressed: () {
                updateIndex();
              },
              //disabledElevation: 2.0,
            ),
          ],
        )),
      ),
    );
  }

  /*void updateIndex() {
    final random = Random();
    final index = random.nextInt(restaurants.length);

    setState(() {
      currentIndex = index;
    });*/
  }
}*/

          /*return LayoutBuilder(builder: (context, constrains) {
            return Scaffold(
              appBar: AppBar(
                flexibleSpace: ClipRRect(
                  clipBehavior: Clip.antiAlias,
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.elliptical(
                          constrains.maxWidth / 2, constrains.minHeight / 2),
                      bottomRight: Radius.elliptical(
                          constrains.maxWidth / 2, constrains.minHeight / 2)),
                ),
              ),
            );
          });*/