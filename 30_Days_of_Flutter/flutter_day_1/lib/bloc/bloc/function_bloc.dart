import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'function_event.dart';
part 'function_state.dart';

class FunctionBloc extends Bloc<FunctionEvent, FunctionState> {
  FunctionState get initialState => const FunctionNextState(0);
  FunctionBloc() : super(const FunctionInitialState()) {
    on<FunctionEvent>((event, emit) {
      if (event is FunctionNextEvent) {
        emit(FunctionNextState(
            (state.currentIndex + 1) % state.restaurants.length));
      }
      if (event is FunctionPrevEvent) {
        emit(FunctionNextState(
            (state.currentIndex - 1) % state.restaurants.length));
      }
    });
  }
}
