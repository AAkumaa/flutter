part of 'function_bloc.dart';

abstract class FunctionState extends Equatable {
  final List<String> restaurants = const ['McDonal\'s', 'FooRocket', 'KFC'];
  // ignore: prefer_typing_uninitialized_variables
  final currentIndex;

  const FunctionState(this.currentIndex);

  @override
  List<Object?> get props => [restaurants, currentIndex];
}

class FunctionInitialState extends FunctionState {
  const FunctionInitialState() : super(0);
}

class FunctionNextState extends FunctionState {
  const FunctionNextState(int currentIndex) : super(currentIndex);

  @override
  List<Object?> get props => [currentIndex];
}

class FunctionPrevState extends FunctionState {
  const FunctionPrevState(int currentIndex) : super(currentIndex);

  @override
  List<Object?> get props => [currentIndex];
}
