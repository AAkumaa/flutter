part of 'function_bloc.dart';

@immutable
abstract class FunctionEvent {}

class FunctionInitialEvent extends FunctionEvent {}

class FunctionNextEvent extends FunctionEvent {}

class FunctionPrevEvent extends FunctionEvent {}
