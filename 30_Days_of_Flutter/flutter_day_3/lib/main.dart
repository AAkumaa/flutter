import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/slider_function_bloc.dart';
import 'ui/home_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => SliderFunctionBloc(),
      child: const MaterialApp(
        home: HomePage(),
      ),
    );
  }
}

/*class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  double _fontSize = 40.0;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      /// SafeArea für die obere Teil der Bildschirm. Somit werden die Informationen nicht versteckt.
      body: SafeArea(
        child: Column(
          children: [
            /// Expanded erlaubt eine saubere erweiterung eines Textfeldes.
            /// Die Tastatur wird nicht verschoben und der TextField wir kontinuerlich erweitert.
            Expanded(
              child: TextField(
                /// initialState ist dafür da, um die Tastatur zu verstecken,
                /// wenn auf "done" drückt.
                textInputAction: TextInputAction.done,
                style: TextStyle(fontSize: _fontSize),

                /// maxLines ist fü+r die TextBruch zuständig. Wenn wir also schreiben,
                /// wird der Text gebrochen und nur auf display gezeigt.
                maxLines: null,
              ),
            ),
            Slider(
              max: 200,
              min: 30,
              value: _fontSize,
              onChanged: (newSize) {
                setState(() => _fontSize = newSize);
              },
            )
          ],
        ),
      ),
    ));
  }
}*/
