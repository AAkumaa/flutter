part of 'slider_function_bloc.dart';

@immutable
abstract class SliderFunctionState {
  final TextEditingController textEditingController;
  final double fontSize;

  SliderFunctionState({
    required this.fontSize,
    required this.textEditingController,
  });

  SliderFunctionState copyWith(
      {required TextEditingController textEditingController,
      required double fontSize}) {
    return SliderFunctionInitial(
      textEditingController:
          textEditingController ?? this.textEditingController,
      fontSize: fontSize ?? this.fontSize,
    );
  }
}

class SliderFunctionInitial extends SliderFunctionState {
  SliderFunctionInitial(
      {required double fontSize,
      required TextEditingController textEditingController})
      : super(
          textEditingController: textEditingController,
          fontSize: fontSize,
        );
}
