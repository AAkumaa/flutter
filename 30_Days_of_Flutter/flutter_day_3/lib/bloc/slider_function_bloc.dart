import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';

part 'slider_function_event.dart';
part 'slider_function_state.dart';

class SliderFunctionBloc
    extends Bloc<SliderFunctionEvent, SliderFunctionState> {
  SliderFunctionBloc()
      : super(SliderFunctionInitial(
            fontSize: 20, textEditingController: TextEditingController())) {
    on<FontSizeEvent>(((event, emit) {
      emit(state.copyWith(
          fontSize: event.fontSize,
          textEditingController: state.textEditingController));
    }));
  }
}
