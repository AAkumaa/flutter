part of 'slider_function_bloc.dart';

@immutable
abstract class SliderFunctionEvent {}

class FontSizeEvent extends SliderFunctionEvent {
  final double fontSize;

  FontSizeEvent(this.fontSize);
}
