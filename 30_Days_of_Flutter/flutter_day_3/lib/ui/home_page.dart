import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_day_1/bloc/slider_function_bloc.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: BlocBuilder<SliderFunctionBloc, SliderFunctionState>(
          builder: (context, state) {
            return Column(
              children: [
                Expanded(
                  child: TextField(
                    textInputAction: TextInputAction.done,
                    controller: state.textEditingController,
                    autocorrect: true,
                    style: TextStyle(fontSize: state.fontSize),
                    maxLines: null,
                  ),
                ),
                Slider(
                    min: 20,
                    max: 100,
                    value: state.fontSize,
                    onChanged: ((double newSize) {
                      BlocProvider.of<SliderFunctionBloc>(context)
                          .add(FontSizeEvent(newSize));
                    }))
              ],
            );
          },
        ),
      ),
    );
  }
}
