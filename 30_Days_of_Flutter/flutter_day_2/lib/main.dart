import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_day_1/bloc/calculate_bloc.dart';
import 'package:flutter_day_1/ui/home_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => CalculateBloc(),
      child: const MaterialApp(
        home: HomePage(),
      ),
    );
  }
}

///Example Code for StatefulWidget

/*class _MyAppState extends State<MyApp> {
  final List<bool> _selection = [true, false, false];
  final controller = TextEditingController();
  String? tip;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              if (tip != null)
                Padding(
                  padding: const EdgeInsets.all(20),
                  child: Text(
                    tip!,
                    style: const TextStyle(fontSize: 30),
                  ),
                ),
              const Text('Total Amount'),
              SizedBox(
                width: 80,
                child: TextField(
                  keyboardType: const TextInputType.numberWithOptions(),
                  controller: controller,
                  textAlign: TextAlign.center,
                  decoration: const InputDecoration(hintText: '\$100.00'),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(20),
                child: ToggleButtons(
                  onPressed: updateSelection,
                  isSelected: _selection,
                  children: const [
                    Text('10%'),
                    Text('15%'),
                    Text('20%'),
                  ],
                ),
              ),
              ElevatedButton(
                onPressed: calculateTip,
                child: const Text('Calculate Amount'),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void updateSelection(int selectIndex) {
    setState(() {
      for (int i = 0; i < _selection.length; i++) {
        _selection[i] = selectIndex == i;
      }
    });
  }

  void calculateTip() {
    final totalAmount = double.parse(controller.text);
    final selectedIndex = _selection.indexWhere((element) => element);
    final tipPercentage = [0.1, 0.15, 0.2][selectedIndex];

    // ignore: unused_local_variable
    final tipTotal = (totalAmount * tipPercentage).toStringAsFixed(2);

    setState(() {
      tip = '\$$tipTotal';
    });
  }
}*/
