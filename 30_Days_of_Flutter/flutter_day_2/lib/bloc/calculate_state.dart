part of 'calculate_bloc.dart';

@immutable
abstract class CalculateState extends Equatable {
  final List<bool> selection;
  final String? tip;
  final String TotalAmount;
  final TextEditingController textEditingController;

  CalculateState(
      {this.tip = '',
      required this.TotalAmount,
      required this.selection,
      required this.textEditingController});

  @override
  List<Object?> get props => [selection, tip, TotalAmount];
}

class CalculateInitial extends CalculateState {
  CalculateInitial(
      {required List<bool> selection,
      String? tip = '',
      required String totalAmount,
      required TextEditingController textEditingController})
      : super(
            selection: selection,
            TotalAmount: totalAmount,
            textEditingController: textEditingController);
}

class CalculatePercentageState extends CalculateState {
  CalculatePercentageState(
      {required List<bool> selection,
      String? tip = '',
      required String totalAmount,
      required TextEditingController textEditingController})
      : super(
            selection: selection,
            TotalAmount: totalAmount,
            tip: tip,
            textEditingController: textEditingController);
}

class UpdateSelectionState extends CalculateState {
  UpdateSelectionState(
      {required List<bool> selection,
      String? tip = '',
      required String totalAmount,
      required TextEditingController textEditingController})
      : super(
            selection: selection,
            TotalAmount: totalAmount,
            tip: tip,
            textEditingController: textEditingController);
}
