part of 'calculate_bloc.dart';

@immutable
abstract class CalculateEvent {}

class CalculatePercentageEvent extends CalculateEvent {}

class UpdateSelectionEvent extends CalculateEvent {
  final int selectIndex;

  UpdateSelectionEvent(this.selectIndex);
}
