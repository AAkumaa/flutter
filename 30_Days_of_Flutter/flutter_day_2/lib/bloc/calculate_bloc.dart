import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

part 'calculate_event.dart';
part 'calculate_state.dart';

class CalculateBloc extends Bloc<CalculateEvent, CalculateState> {
  ///Constructor
  CalculateBloc()
      : super(CalculateInitial(
            selection: [true, false, false],
            totalAmount: '',
            textEditingController: TextEditingController())) {
    ///On-Method to emit the State
    on<CalculateEvent>((event, emit) {
      ///UpdateSelectionEvent
      if (event is UpdateSelectionEvent) {
        ///emit the Index of the button-press
        final updateSelect = List.of(state.selection);

        for (int i = 0; i < updateSelect.length; i++) {
          updateSelect[i] = event.selectIndex == i;
        }

        emit(UpdateSelectionState(
            selection: updateSelect,
            totalAmount: state.TotalAmount,
            tip: state.tip,
            textEditingController: state.textEditingController));
      }

      ///CalculatePercentageEvent
      if (event is CalculatePercentageEvent) {
        final controller = state.textEditingController;
        final totalAmount = double.parse(controller.text);
        final selectedIndex = state.selection.indexWhere((element) => element);
        final tipPercentage = [0.1, 0.15, 0.2][selectedIndex];
        final tipTotal = (totalAmount * tipPercentage).toStringAsFixed(2);
        emit(CalculatePercentageState(
            selection: state.selection,
            totalAmount: totalAmount.toString(),
            tip: tipTotal,
            textEditingController: state.textEditingController));
      }
    });
  }
}
