import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_day_1/bloc/calculate_bloc.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            BlocBuilder<CalculateBloc, CalculateState>(
                bloc: BlocProvider.of<CalculateBloc>(context),
                builder: (context, state) {
                  if (state.tip != null) {
                    return Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(20),
                          child: Text(state.tip ?? '',
                              style: const TextStyle(fontSize: 30)),
                        ),
                        const Text('Total Amount'),
                        SizedBox(
                          width: 80,
                          child: TextField(
                            controller: state.textEditingController,
                            keyboardType:
                                const TextInputType.numberWithOptions(),
                            textAlign: TextAlign.center,
                            decoration:
                                const InputDecoration(hintText: '\$100.00'),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(20),
                          child: ToggleButtons(
                            isSelected: state.selection,
                            onPressed: (index) =>
                                BlocProvider.of<CalculateBloc>(context)
                                    .add(UpdateSelectionEvent(index)),
                            children: const [
                              Text('10%'),
                              Text('15%'),
                              Text('20%'),
                            ],
                          ),
                        ),
                      ],
                    );
                  } else {
                    return Container(
                      child: const Text('flase'),
                    );
                  }
                }),
            ElevatedButton(
              onPressed: () {
                BlocProvider.of<CalculateBloc>(context)
                    .add(CalculatePercentageEvent());
              },
              child: const Text('Calculate Amount'),
            ),
          ],
        ),
      ),
    );
  }
}
